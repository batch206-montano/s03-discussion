import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        //System.out.println("Hello world!");

        //Loops are control structures taht allow code blocks to be repeated according to conditions set
        //Types of Loops
        //While loop
        //Do-While loop
        //For Loop
        //The enhanced For Loop for Arrays and ArrayLists

        //While Loops - allow us to run/repeat actions/codes based on a given condition

        int a = 1;

        while(a < 5){
            System.out.println("While Loop Counter: " + a);
            a++;
        }

        Scanner userInput = new Scanner(System.in);




        boolean hasNoInput = true;
        while(hasNoInput){

            System.out.println("Enter your name:");
            String name = userInput.nextLine();

            if(name.isEmpty()){
                System.out.println("Please try again");
            } else {
                hasNoInput = false;
                System.out.println("Thank you for your input");
            }
        }

        //What is the difference between do-while and while loop?

        int b = 5;

        do {
            System.out.println("Countdown: " + b);
            b--;
        } while (b >= 1);

        //For Loops - more versatile and more commonly used
        for(int i = 1; i <= 10; i++){
            System.out.println("Count: " + i);
        }

        //For Loop over a Java Array
        int[] intArray = {100,200,300,400,500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item at index number " + i + " is " + intArray[i]);
        }

        //Loop Over Multidimensional Array
        //Multidimensional Arrays
        //are arrays nested within each other.
        //The first array could be for the rows, the second could be for cols
        String[][] classroom = new String[3][3];

        //First Row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        //Second Row
        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        //third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.toString(classroom));
        //Is used to display the values of a multidimensional array
        System.out.println(Arrays.deepToString(classroom));

        //Loop Over All the Items in the Multidimensional array
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        //Enhanced For Loop for Java Array/ArrayList
        //In Java, we can use an enhanced for loop to loop over EACH item in an array or arraylist.
        //for-each in Java Array and ArrayList is also called the enhanced for loop

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        //member is a parameter representing an item in the members array. Even as a parameter, you have to indicate the data type.
        for(String member: members){
            System.out.println(member);
        }

        //Enhanced For Loop on Multidimensional Arr
        for(String[] row: classroom){
            //row - each array
            //System.out.println(Arrays.toString(row));
            for(String student: row){
                System.out.println(student);
            }
        }

        //HashMap forEach
        //HashMap has a method for iterating each field-value pair
        //The HashMap forEach() requires a lambda expression as an argument
        //A lambda expression in Java, is a short block of code which takes in parameters and returns a value. Lambda expressions are similar to methods, but they do not have a name and are implemented within another method.

        HashMap<String,String> techniques = new HashMap<>();
        techniques.put(members[0],"Spirit Gun");
        techniques.put(members[1],"Black Dragon");
        techniques.put(members[2],"Rose Whip");
        techniques.put(members[3],"Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key,value) -> {

            System.out.println("Member " + key + " uses " + value);

        });

        //Exception Handling

        //int numberSample = "25000";

//        System.out.println("Enter an integer:");
//
//        int num = userInput.nextInt();
//
//        System.out.println(num);

        //Exceptions are errors that happens during run-time, this means that the program has been compiled and running but an unexpected error happened. WE should be able to handle exceptions to not let our program end abnormally because of a run-time error.
        //Exception Handling refers to managing and catching run-time errors in order to safely run our code and do not stop abnormally.
        //For the use of scanner we should handle exceptions because we cannot always anticipate the input of our users.

        System.out.println("Enter an integer");
        int num = 0;

        //try-catch-finally - try-catch statement allow us to catch exceptions in our code.

        try {
            num = userInput.nextInt();
        } catch (Exception e){
            //Show a message at least about catching an exception
            System.out.println("Invalid Input");
        }

        System.out.println("Hello from the other side!");

    }
}
